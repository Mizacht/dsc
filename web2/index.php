<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

	<title>Web2</title>
</head>
<body>
	
	<!-- START CODE FOR TITLE-->
	<div class="container">
	    <div class="row">
	      <h2>DATA STUDENT</h2>
	    </div>
	</div>
	<!-- ENDCODE FOR TITLE-->

	<!-- START CODE FOR FORM -->
	<div class="container">
	    <div class="row">
	    <form action="save.php" method="POST">
	    <div class="form-group">
	        <label>Name</label>
	          <input type="text" class="form-control" name="name"/>
	     </div>
	     <div class="form-group">
	        <label>Faculty</label>
	        <select class="form-control" name="faculty">
	            <option value="">- Select Faculty -</option>
	            <option value="Computer Engineering">Computer Engineering</option>
	            <option value="Design">Design</option>
	         </select>
	     </div>
	     <div class="form-group">
	        <label>IPK</label>
	          <input type="text" class="form-control" name="ipk"/>
	     </div>
	     <button class="form-control" type="submit">Save</button>
	     </form>
	    </div>
	</div>
	<!-- END CODE FOR FORM -->

	<!-- START CODE FOR TABLE -->
	<div class="container">
	    <div class="row"> 
	        <table class="table">
	           <thead>
	                 <tr>
	                    <th>ID</th>
	                    <th>Name</th>
	                    <th>Faculty</th>
	                    <th>IPK</th>
	                    <th>Action</th>
	                  </tr>
	            </thead>
	       </table>
	</row>
	</div>
<!-- END CODE FOR TABLE -->

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</body>
</html>