<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Web1</title>
  </head>
  <body>
    <!-- Navbar -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Cat Cafe</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Cat</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About</a>
              </li>
          </div>
        </nav>
        </div>
      </div>
    </div>

    <!-- Carousel & side list -->
    <div class="container" style="margin-top: 10px">
      <div class="row">
        <!-- carousel -->
        <div class="col-lg-8">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="img/banner1.jpg" alt="First slide" style="height: 400px">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="img/banner2.jpg" alt="Second slide" style="height: 400px">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="img/banner3.jpg" alt="Third slide" style="height: 400px">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        
        <!-- side list -->
        <div class="col-lg-4">
          <h2>Cat Type List</h2>
          <ul class="list-group">
            <li class="list-group-item">Persian</li>
            <li class="list-group-item">Bengal</li>
            <li class="list-group-item">British Shorthair</li>
            <li class="list-group-item">Maine Coon</li>
            <li class="list-group-item">Birman</li>
          </ul>
        </div>
      </div>
    </div>

    <!-- Random Words -->
    <div class="container" style="margin-top: 10px">
      <div class="row">
        <div class="col-lg-12">
          <h5>Here some Catnip</h5>
        </div>
      </div>
    </div>

    <!-- Cards -->
    <div class="container" style="margin-top: 10px">
      <div class="row">
        <div class="col-lg-4">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/card1.jpg" alt="Card image cap" style="height: 200px">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/card2.jpeg" alt="Card image cap" style="height: 200px">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="img/card3.jpg" alt="Card image cap" style="height: 200px">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <a href="#" class="btn btn-primary">Go somewhere</a>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  </body>
</html>